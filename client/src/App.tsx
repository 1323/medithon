import React, { Component } from 'react';
import { SemanticToastContainer, toast } from 'react-semantic-toasts';
import {Provider} from 'react-redux';
import {I18nextProvider} from 'react-i18next';
import apolloClient from  "./services/config/apollo";
import { ApolloProvider } from 'react-apollo';
// import { Router, Route, route  } from 'react-router';
import {
  BrowserRouter as Router, Route, NavLink, Switch, Link
} from 'react-router-dom';
import Footer from './components/footer';
import style from './app.scss';
import {SidebarContainer, HeaderContainer, Dashboard, PhotoFoot} from './components/containers';
import Main from './routes/main';
import store from './redux'
import Profile from './routes/profile';
import i18n from "./i18n";
import {checkAuthThunk} from './redux/actions'
import { browserHistory } from './utils';
import 'react-semantic-toasts/styles/react-semantic-alert.css';
// import Dashobard from './components/containers/Dashobard';


export interface IProps {
	history: any;
}

export interface IState {
	isDrawerVisible: boolean;
}

export default class App extends Component {
	public componentDidMount() {
		store.dispatch(checkAuthThunk())
	}
	
	public render() {
		return (
			<I18nextProvider i18n={i18n}>
				<ApolloProvider client={apolloClient}>
					<Provider store={store}>
					<Router>
							<SidebarContainer>
										<>
											<HeaderContainer/>
											<div className={style.maincontent}>
											<SemanticToastContainer position={'bottom-right'} />
												<Switch>
														<Route exact path="/" component={Main} />
														<Route exact path="/profile" component={Profile} />
														<Route path="/profile/:access_token?/:expires_in?/:token_type?/:state?" component={Profile} />
														<Route path="/photo" component={PhotoFoot}/>
														<Route path="/home" component={Dashboard}  />
												</Switch>
											</div>
										</>
								<Footer/>
							</SidebarContainer>
						</Router>
					</Provider>
				</ApolloProvider>
			</I18nextProvider>
		);
	}
}
