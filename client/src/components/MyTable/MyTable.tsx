import * as React from "react";
import {Table} from "semantic-ui-react";
// import { Field } from "formik"; 
import styles from './styles.scss';

interface IProps {
  headerData : any[],
  bodyData : any[any],
  footerData : any[],
  onSelectedItem?: any,
  columns : any
}
interface IState {
  focusedId : any
}

class MyTable extends React.Component < IProps,
IState > {
  public eventValue = 0;
  public state = {
    focusedId: null
  };

  public onClick = (id) => {
    const { onSelectedItem } = this.props;
    this.setState({focusedId: id});
    // tslint:disable-next-line:no-unused-expression
    onSelectedItem && onSelectedItem(this.props.bodyData[id]);
  }

  public render() {
    const {
      headerData = [],
      bodyData = [],
      footerData = [],
      columns = 3
    } = this.props;
    const {
      focusedId
    } = this.state;
    return (
      <div className={styles.tableContainer}>
      <Table selectable={true} anylumns={columns}>
        <Table.Header>
          <Table.Row>
            {headerData.map((item, id) => (
              <Table.HeaderCell key={id}>{item}</Table.HeaderCell>
            ))}
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {(bodyData).map((item, id) => (
            // tslint:disable-next-line:jsx-no-lambda
            <Table.Row key={id} active={id === focusedId} onClick={() => this.onClick(id)}>
              {Object
                .values(item)
                .map(val => (
                  <Table.Cell>{val}</Table.Cell>
                ))}
            </Table.Row>
          ))}
        </Table.Body>

        <Table.Footer>
          <Table.Row>
            {footerData.map((item, id) => (
              <Table.HeaderCell>{item}</Table.HeaderCell>
            ))}
          </Table.Row>
        </Table.Footer>
      </Table>
      </div>
    );
  }
}

export default MyTable;