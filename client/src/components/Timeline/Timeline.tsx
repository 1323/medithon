// // tslint:disable:ordered-imports
// // tslint:disable:object-literal-sort-keys
// import moment from 'moment';
// import * as React from 'react';

// import { Button, Dropdown, Form } from 'formik-semantic-ui';
// import { fetchServicesThunk, updateServiceThunk, createServiceThunk } from '../../redux/actions/';
// import { connect } from 'react-redux';
// import { Image } from 'semantic-ui-react';
// import Timeline from 'react-calendar-timeline'
// // import styles from './Timeline.scss';
// import 'react-calendar-timeline/lib/Timeline.css'
// import './Timeline.scss'
// import generateFakeData from "./dataGenerator.js";
// import ModalHoc from '../../modals/modalHOC';
// import ServiceForm from '../../forms/Service';


// const keys = {
//   groupIdKey: "id",
//   groupTitleKey: "title",
//   groupRightTitleKey: "rightTitle",
//   itemIdKey: "id",
//   itemTitleKey: "title",
//   itemDivTitleKey: "title",
//   itemGroupKey: "group",
//   itemTimeStartKey: "start",
//   itemTimeEndKey: "end",
//   groupLabelKey: "title"
// };

// interface IState {
//   groups:any, defaultTimeStart:any, defaultTimeEnd:any
// }

// interface IProps {
//   services:any
//   fetchServices:any
//   createService:any
//   updateService:any
// }

// class Timelines extends React.Component<IProps, IState> {
//   public items: Array<{ canResize: boolean; className: string; end: number; group: number; id: string; itemProps: { [x: string]: any; 'data-tip': any; }; start: number; title: string; }>;

//   constructor(props) {
//     super(props);
//     // tslint:disable:no-shadowed-variable
//     const { groups, items } = generateFakeData();
//     const defaultTimeStart = moment()
//       .startOf("day")
//       .toDate();
//     const defaultTimeEnd = moment()
//       .startOf("day")
//       .add(1, "day")
//       .toDate();
//     this.items = items;
//     this.state = {
//       groups,
//       defaultTimeStart,
//       defaultTimeEnd
//     };
//     moment.locale('pl')
//   }

//   public componentDidMount() {
//     this.props.fetchServices(this.items);
//   }

//   public handleItemMove = (itemId, dragTime, newGroupOrder) => {
//     this.props.updateService(itemId, dragTime, newGroupOrder);
//   }


//   public filters = () => 
//   <Form
// 		initialValues={{
// 			barber: 1,
// 			service: 0,
//     }}
//     className="barberForm"
// 		// tslint:disable-next-line:jsx-no-lambda
// 		onSubmit={(values: any, { setSubmitting }: any) => {
// 			setTimeout(() => {
// 				console.log('values', values);
// 				// addProduct(values)
// 				setSubmitting(false);
// 			}, 500);
// 		}}
// 		// tslint:disable-next-line:jsx-no-lambda
// 		render={() => (<Form.Children>
//       <Form.Group widths="3">
//         <Dropdown
//           label="fryzjer"
//           name="barber"
//           options={[ { text: 'Krysia', value: 1 }, { text: 'Halina', value: 2 }, { text: 'Gabi', value: 3 }, { text: 'Grażynka', value: 4 } ]}
//         />
//         <Dropdown
//           label="usługa"
//           name="service"
//           options={[ { text: 'strzyżenie męskie', value: 0 }, { text: 'strzyżenie damskie', value: 1 } ]}
//         />
//         <Button.Submit className="submitBarber"> Szukaj </Button.Submit>
//       </Form.Group>
//     </Form.Children>)
//     }
// 	/>
  

//   public render() {
//     const { groups,  defaultTimeStart, defaultTimeEnd } = this.state;
//     console.log('Timeline services', this.props.services);

//     return (
//       <div>
//         <div className="servicePanel">
//           {this.filters()}
//           <ModalHoc 
//                 buttonTitle="zarezerwuj"
//                 headerTitle="nowa rezerwacja"
//                 buttonColor="#abc"
//                 // tslint:disable-next-line:jsx-no-lambda
//                 render={ ({ close }) => (
//                   <Form
//                   initialValues={{
//                     clientName: '',
//                     groupId: 1,
//                     serviceType: 0,
//                     startsAt: moment(),
//                   }}
//                   // tslint:disable-next-line:jsx-no-lambda
//                   onSubmit={(values: any, { setSubmitting }: any) => {
//                     setTimeout(() => {
//                       const { createService } = this.props;
//                       console.log('values', values);
//                       const { groupId: group, serviceType: type, startsAt: start } = values;
//                       createService({group, type, start: start.valueOf()})
//                       setSubmitting(false);
//                       close();
//                     }, 500);
//                   }}
//                   render={() => <ServiceForm />}
//                   />)
//                 }
//           />
//         </div>
//         {this.props.services.length && <Timeline
//           groups={groups}
//           items={this.props.services}
//           keys={keys}
//           sidebarContent={<div>&nbsp;</div>}
//           itemsSorted={true}
//           itemTouchSendsClick={false}
//           stackItems={true}
//           itemHeightRatio={0.93}
//           showCursorLine={true}
//           canMove={false}
//           defaultTimeStart={defaultTimeStart}
//           defaultTimeEnd={defaultTimeEnd}
//           lineHeight={100}
//           sidebarWidth={80}
//           onItemMove={this.handleItemMove}
//           // tslint:disable-next-line:jsx-no-lambda
//           itemRenderer={({
//             item,
//             itemContext,
//             getItemProps,
//             getResizeProps
//           }) => {
//             const { left: leftResizeProps, right: rightResizeProps } = getResizeProps()
//             const newItem = getItemProps({...item.itemProps});

//             // todo make it more readable
//             newItem.style.background = item.type === 0 ? '#85baff' : 'ffca85';
//             newItem.style.opacity = itemContext.selected ? '0.66' : '1';
//             newItem.style.borderRadius = '5px';
//             newItem.style.border = 'none';
//             return (
//               <div {...newItem}>
//                 {itemContext.useResizeHandle ? <div {...leftResizeProps} /> : ''}
//                 <div
//                   className="rct-item-content"
//                   style={{ maxHeight: `${itemContext.dimensions.height}`}}
//                 >
//                   {itemContext.title}
//                 </div>
          
//                 {itemContext.useResizeHandle ? <div {...rightResizeProps} /> : ''}
//               </div>
//             )}}
//           // tslint:disable-next-line:jsx-no-lambda
//           groupRenderer={({group})=> {
//             return (
//               <div className="custom-group">
//                 <Image
//                   avatar={true}
//                   src={`/assets/${group.img || 'b'}.png`}
//                 />
//                 <span>{group.name}</span>
//               </div>
//             )
//           }}
//           // tslint:disable-next-line:jsx-no-lambda
//           // onCanvasClick={(groupId, time, e) => {
//           //   console.log('groupId, time, e', groupId, time, e);
//           // }}
//           // minResizeWidth={80}
//         />}
//       </div>
//     );
//   }
// }

// const mapDispatchToProps = (dispatch) => {
// 	return {
// 		createService: (...input) => {
// 			dispatch(createService(...input));
// 		},
// 		fetchServices: (items) => {
// 			dispatch(fetchServices(items));
// 		},
// 		updateService: (...input) => {
// 			dispatch(updateService(...input));
// 		},
// 	};
// };

// const mapStateToProps = (state) => 
// 	({services: state.services.services})

// export default connect(mapStateToProps, mapDispatchToProps)((
// 	Timelines
// ) as any);