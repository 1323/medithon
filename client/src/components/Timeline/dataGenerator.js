// eslint:disable:object-literal-sort-keys
import faker from 'faker';
import randomColor from 'randomcolor';
import * as moment from 'moment';
const names = [ 'Krysia', 'Gabrysia', 'Halina', 'Grażynka' ];
const tasks = [ 'strzyżenie meskie', 'strzyżenie damskie', 'farbowanie', 'kreatynowanie', 'przerwa' ];

const getTask = (id) => {
	return tasks[id];
};
export default function(groupCount = 4, itemCount = 4, daysInPast = 30) {
	const randomSeed = Math.floor(Math.random() * 1000);
	const groups = [];
	const img = [ 'a', 'b', 'c', 'd' ];

	for (let i = 0; i < groupCount; i++) {
		groups.push({
			bgColor    : randomColor({
				luminosity : 'light',
				seed       : randomSeed + i
			}),
			id         : `${i + 1}`,
			img        : img[i],
			name       : names[i],
			rightTitle : faker.name.lastName()
		});
	}

	let items = [];
	for (let i = 0; i <= itemCount; i++) {
		// const startDate = faker.date.recent(daysInPast).valueOf() + daysInPast * 0.3 * 86400 * 1000;
		const startDate = new Date();
		const startValue = Math.floor(moment(startDate).valueOf() / 10000000) * 10000000;
		const typeTimings = [ 1800000, 3600000, 5400000, 2400000 ];
		const randomSerivceId = faker.random.number() % typeTimings.length;
		items.push({
			// canMove: startValue > new Date().getTime(), canResize: startValue > new
			// Date().getTime() ? (endValue > new Date().getTime() ? 'both' : 'left') :
			// (endValue > new Date().getTime() ? 'right' : false),
			canResize : false,
			className : moment(startDate).day() === 6 || moment(startDate).day() === 0 ? 'item-weekend' : '',
			end       : startValue + typeTimings[randomSerivceId],
			group     : i,
			id        : i + '',
			itemProps : {
				'data-tip' : faker.hacker.phrase()
			},
			start     : startValue,
			title     : getTask(randomSerivceId),
			type      : randomSerivceId
		});
	}

	items = items.sort((a, b) => b - a);
	// console.log('items', JSON.stringify(items));
	return { groups, items };
}
