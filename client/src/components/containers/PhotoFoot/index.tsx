import React, { Component } from 'react';
import { Container, Button, Divider, Header } from 'semantic-ui-react';
import { translate } from 'react-i18next';
import { withRouter } from 'react-router-dom';
export interface IProps {
	path?: string;
}

export interface ITranslateProps {
	t? : (s) => string;
	i18n?: any;
}

class PhotoFoot extends Component<IProps & ITranslateProps, {}> {
	public render() {
		const { t, i18n} = this.props
		return (
			<Container>
						{''}
			</Container>
		);
	}
}

export default withRouter(translate('translations')(PhotoFoot)) as React.ComponentClass<IProps & ITranslateProps>;