import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect, withRouter, Link, NavLink} from 'react-router-dom';
import {Container, Responsive, Menu, Icon} from 'semantic-ui-react';
// import {auth0Thunk, unAuth0Thunk, toggleDrawerThunk} from '../../../redux/actions/'
// import {Link}from 'react-router-dom';
import style from './style.scss';
import {translate} from "react-i18next";

export interface IProps {
  onLogin?: any;
  onLogout : (s) => string;
  toggleSidebar : any;
  history : any;
  t : (s) => string;
  user : any;
}

interface IState {
  activeItem : any;
}

class Header extends Component < IProps,
IState > {
  // tslint:disable-next-line:member-access
  state = {
    activeItem: ''
  };

  public handleItemClick = (e, { name }) => {this.setState({ activeItem: name })}

  public render() {
    const {activeItem} = this.state;
    const {toggleSidebar, user, onLogin, onLogout, t} = this.props;
    return (
      <header className={style.header}>
        <Container>
          <NavLink to={'/'}>
            <h1>ProDiabetica</h1>
          </NavLink>
          <nav className={style.nav}>
            <Responsive
              as={Menu.Menu}
              position='right'
              minWidth={Responsive.onlyTablet.minWidth}>
              <Menu.Item
                content={t('poziom glukozy')}
                onClick={this.handleItemClick}
                name="home"
                as={NavLink}
                to="/home"
                exact
                active={activeItem === 'home'}
                />
              <Menu.Item
                content={t('stopa cukrzycowa')}
                onClick={this.handleItemClick}
                name="fields"
                exact
                active={activeItem === 'fields'}
                as={NavLink}
                to="/fields"
                />
              <Menu.Item content={t('menu')} name="about" link onClick={toggleSidebar}/>
            </Responsive>
            <Responsive as={Menu.Menu} {...Responsive.onlyMobile}>
              <Menu.Item link name="hamburg" onClick={toggleSidebar}>
                <Icon inverted name="sidebar"/>
              </Menu.Item>
            </Responsive>
          </nav>
        </Container>
      </header>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onLogin: () => {
      // dispatch(auth0Thunk())
    },
    onLogout: () => {
      // dispatch(unAuth0Thunk())
    },
    toggleSidebar: () => {
      // dispatch(toggleDrawerThunk({}, 'type'))
    }
  }
}
const mapStateToProps = (state : any) => ({user: state.user});
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(translate("translations")(Header)as any));