import React, {Component} from 'react';
import {
		Sidebar,
		Responsive,
		Segment,
		Button,
		Flag,
		Menu,
		Image,
		Icon
} from 'semantic-ui-react';
import {Redirect, NavLink} from 'react-router-dom';
import {connect} from 'react-redux';
import {toggleDrawerThunk } from '../../../redux/actions/';
	// auth0Thunk, unAuth0Thunk} from '../../../redux/actions/'
import {translate} from "react-i18next";
import util from '../../../utils/subsManager';
import style from './style.scss';
import classNames from 'classnames/bind';
import {withRouter} from 'react-router-dom';

interface IProps {
		children : any,
		i18n?: any,
		user?: any,
		isOpen : boolean,
		onLogin : any;
		onLogout : (s) => string;
		toggleSidebar?: any,
		history?: any,
		t : any
}

const mapDispatchToProps = {
		toggleSidebar: toggleDrawerThunk,
		// onLogin: auth0Thunk,
		// onLogout: unAuth0Thunk
};
const mapStateToProps = (state : any) => ({isOpen: state.drawer.isOpen, user: state.user});

@connect(mapStateToProps, mapDispatchToProps)

class SidebarLeftOverlay extends Component < IProps, {} > {

		public ButtonExampleConditionals = (i18n) => {
				return (
						<div className={classNames('icon', 'lang-picker')}>
								<Flag
										name='pl'
										className={i18n.language !== 'pl'
										? style.inactive
										: ''}
										onClick={() => {
										this
												.props
												.i18n
												.changeLanguage('pl')
								}}/>
								<Flag
										name='gb'
										className={i18n.language !== 'eng'
										? style.inactive
										: ''}
										onClick={() => this.props.i18n.changeLanguage('eng')}/>
						</div>
				)
		}

		public render() {
				const {
						t,
						i18n,
						isOpen,
						user,
						onLogin,
						onLogout,
						children,
						toggleSidebar
				} = this.props;
				return (
						<Sidebar.Pushable
								as={Menu}
								style={{
								margin: "0",
								border: 'none'
						}}>
								<Sidebar
										as={Menu}
										animation="overlay"
										width="thin"
										visible={isOpen}
										icon="labeled"
										vertical
										inverted>
										<div>
												<Responsive as={Menu.Menu} {...Responsive.onlyMobile}>
														<Menu.Item name="home" to="/home" as={NavLink}>
																<Icon name="gamepad"/> {t('games')}
														</Menu.Item>
														<Menu.Item name="fields" to="/fields" as={NavLink}>
																<Icon name="home"/> {t('fields')}
														</Menu.Item>
														{!user.isLogged
																? <Menu.Item onClick={onLogin}>
																				<Icon name="camera"/> {t('login')}
																		</Menu.Item>
																: <Menu.Item name="camera" onClick={onLogout}>
																		<Icon name="camera"/> {t('logout')}
																</Menu.Item>
														}
												</Responsive>
												<Menu.Item name="camera">
														{this.ButtonExampleConditionals(i18n)}
														{t('Language')}
												</Menu.Item>
												<Menu.Item
														name="bell"
														link
														onClick={() => util.create((subId) => {
														util.toggle(subId, (r) => {
																console.log('r', r);
														});
												})}><Icon name="bell" color="yellow"/> {t('Notifications')}
												</Menu.Item>
												{user.isLogged && <Menu.Item name="user" to="/profile" as={NavLink}>
														<Icon name="user"/> {t('Profile')}
												</Menu.Item>}
										</div>
								</Sidebar>
								<Sidebar.Pusher
										dimmed={isOpen}
										onClick={isOpen && toggleSidebar}
										style={{
										minHeight: "100vh",
										width: "100%"
								}}>
										{children}
								</Sidebar.Pusher>
						</Sidebar.Pushable>
				);
		}
}

export default withRouter(translate("translations")(SidebarLeftOverlay));