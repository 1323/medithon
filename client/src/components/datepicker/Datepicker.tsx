import React,  {Component} from 'react';
import moment from 'moment'
import { connect, getIn } from 'formik';
import { Form } from 'semantic-ui-react'
import DatePickerComponent from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import './styles.scss'
import { get } from './helper';
export interface IProps{
  className?: any;
  dateFormat : any;
  disabled?: any;
  hint? : any;
  label?: any;
  maxDate?: any;
  minDate?: any;
  minTime?: any;
  maxTime?: any;
  name?: any;
  placeholder?: any;
  required?: any;
  formik: any;
  stylingProps?: any;
  inputProps?: any;
}
moment.locale('pl');

class Datepicker extends Component <IProps, {}> {
  
  public handleChangeRaw = e => {
    const { formik } = this.props
    const { name, value } = e.target

    const validChars = /^\d{0,2}[.]{0,1}\d{0,2}[.]{0,1}\d{0,4}$/
    if (!validChars.test(value)) {
      e.preventDefault()

      return
    }

    const momentDate = moment(
      value,
      this.props.dateFormat,
      true,
    )

    const updatedValue = momentDate.isValid() ? momentDate.format('YYYY-MMhandleChange-DD') : ''

    formik.setFieldValue(name, updatedValue)
    formik.setFieldTouched(name, true)
  }

  public handleChange = momentDate => {
    const { formik } = this.props;
    const { name } = this.props
    formik.setFieldValue(name, momentDate)
    formik.setFieldTouched(name, true)
  }

  public handleFocus = name => () => {
    // document.getElementById(name).focus()
  }

  public render() {
    const {
      className,
      dateFormat,
      disabled,
      hint,
      label,
      maxDate,
      minDate,
      minTime,
      maxTime,
      name,
      placeholder,
      required,
      formik,
      stylingProps
    } = this.props

    // const { formik } = this.context
    // const { touched, errors, values } = formik
    // const errors = getIn(formik.errors, name);
    // const touched = getIn(formik.touched, name);
    const values = getIn(formik.values, name);
    // const error = get(touched, name) && get(errors, name)
    return (
      <Form.Field className={stylingProps}>
        {
          label &&
          <label
            onClick={this.handleFocus(name)}
            onKeyPress={this.handleFocus(name)}
            role="none"
          >
            {`${label}${required ? ' *' : ''}`}
          </label>
        }
        <DatePickerComponent
          selected={values ? values : null}
          placeholderText={placeholder || ''}
          timeFormat="HH:mm"
          minDate={minDate}
          minTime={minTime}
          maxTime={maxTime}
          dateFormat={dateFormat}
          timeIntervals={30}
          showTimeSelect
          onChange={this.handleChange}
          disabled={disabled}
          hint={hint}
        />
        {/* {
          error &&
            <span className="error">
              {error}
            </span>
        }
        {
          hint &&
          <span className="hint">
            { hint}
          </span>
        } */}
      </Form.Field>
    )
  }
}
export default connect(Datepicker)
