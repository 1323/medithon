import React, {PureComponent} from 'react';
import {
    Header,
    Grid,
    List,
    Segment,
} from 'semantic-ui-react';
import {Container} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {translate} from "react-i18next";

export interface IProps {
    t : any;
    submitForm : any;
}

const Footer : React.StatelessComponent < IProps > = ({t, submitForm}) => (
    <Segment inverted vertical style={{
        padding: '5em 0em'
    }}>
        <Container>
        {}
        </Container>
    </Segment>
)

const mapDispatchToProps = (dispatch) => {
    return {
        submitForm: (data) => {
        }
    }
}
export default connect(null, mapDispatchToProps)(translate("translations")(Footer)as any);;