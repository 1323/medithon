// import { Button, Dropdown, Form, Input } from 'formik-semantic-ui';
// import moment from 'moment';

// import * as React from 'react';
// import { connect } from 'react-redux';
// import { Table } from 'semantic-ui-react';
// // import AddProdToOrderModal from '../modals/addToOrder';
// import AddSupplier from '../modals/addSupplier';
// import AddToOrder from '../modals/addToOrder/addToOrder';
// import ModalHoc from '../modals/modalHOC';
// import { totalSelector } from '../redux/selectors/invoice';
// import { DatePicker } from './components/DatePicker';
// import styles from './Invoice.scss';

// interface IValues {
// 	invoiceNumber: string;
// 	supplier: string;
// 	invoiceDate: Date;
// } 

// const Invoice: React.SFC<{orders, total, handleReset}> = ({ orders, total, handleReset }) => (
// 	<Form.Children>
// 		<Form.Group widths="2">
// 			<Input label="Number" name="invoiceNumber" placeholder="John" type="text" />
// 			<DatePicker
// 				label="Date of Accountance"
// 				name="invoiceDate"
// 				inputProps={{
// 					isOutsideRange: (day) => !moment(day).isSameOrBefore(moment()),
// 					renderMonthElement: (props) => <DatePicker.YearMonthSelector {...props} />
// 				}}
// 			/>
// 		</Form.Group>

// 		<Form.Group widths="2">
// 			<Dropdown
// 				label="Supplier"
// 				name="supplier"
// 				inputProps={ { 
// 					onChange: (...sth) => {console.log(sth);},
// 				}}
// 				// tslint:disable-next-line:jsx-no-lambda
// 				options={[ { text: 'Bakery', value: 'B' }, { text: 'Groccery', value: 'G1' } ]}
// 			/>
// 			<div className={styles.addButton}>
// 				<ModalHoc 
// 							buttonTitle="add Supplier"
// 							headerTitle="new Supplier"
// 							// tslint:disable-next-line:jsx-no-lambda
// 						 	render={ ({ close }) => (<AddSupplier onCloseModal={close} />) }
// 				/>
// 			</div>
// 		</Form.Group>

// 		<Table columns={5}>
// 			<Table.Header>
// 				<Table.Row>
// 					<Table.HeaderCell>Name</Table.HeaderCell>
// 					<Table.HeaderCell>Tax</Table.HeaderCell>
// 					<Table.HeaderCell>Quantity</Table.HeaderCell>
// 					<Table.HeaderCell>Price</Table.HeaderCell>
// 					<Table.HeaderCell>Value</Table.HeaderCell>
// 				</Table.Row>
// 			</Table.Header>

// 			<Table.Body>
// 				{	(orders.length > 0) && orders.map(item => (
// 					<Table.Row>
// 					<Table.Cell>{item.productName}</Table.Cell>
// 					<Table.Cell>{item.tax}</Table.Cell>
// 					<Table.Cell>{item.quantity}</Table.Cell>
// 					<Table.Cell>{item.price}</Table.Cell>
// 					<Table.Cell>{item.orderValue}</Table.Cell>
// 					</Table.Row>
// 				))}
// 			</Table.Body>

// 			<Table.Footer>
// 				<Table.Row>
// 					<Table.HeaderCell>total : </Table.HeaderCell>
// 					<Table.HeaderCell>{total}</Table.HeaderCell>
// 					<Table.HeaderCell />
// 					<Table.HeaderCell />
// 					<Table.HeaderCell>
// 						<ModalHoc 
// 							buttonTitle="add orders"
// 							headerTitle="new order"
// 							// tslint:disable-next-line:jsx-no-lambda
// 						 	render={ ({ close }) => (<AddToOrder onCloseModal={close} />) }
// 						/>
// 					</Table.HeaderCell>
// 				</Table.Row>
// 			</Table.Footer>
// 		</Table>
// 		<Form.Group widths="2">
// 		<Button.Submit primary={true}>Submit</Button.Submit>
// 		<Button basic={true} onClick={handleReset}>
// 			Cancel
// 		</Button>
// 		</Form.Group>
		
// 	</Form.Children>
// );

// const Invoices: React.SFC<{orders, total}> = ({orders, total}) => (
// 	<Form.Children>
// 		<h2>new Invoice</h2>
// 		<Form
// 			initialValues={{
// 				invoiceDate: new Date(),
// 				invoiceNumber: '',
// 				supplier: ''
// 			}}
// 			// tslint:disable-next-line:jsx-no-lambda
// 			onSubmit={(values: IValues, { setSubmitting }: any) => {
// 				setTimeout(() => {
// 					// alert(JSON.stringify(values, null, 2));
// 					setSubmitting(false);
// 				}, 500);
// 			}}
// 			// tslint:disable-next-line:jsx-no-lambda
// 			render={(props) => <Invoice orders={orders} total={total} handleReset={props.handleReset} />}
// 		/>
// 	</Form.Children>
// );


// const mapDispatchToProps = (dispatch) => {
// 	return {
// 	};
// };

// const mapStateToProps = (state) =>{
// 	return (
// 	{orders: state.invoice.orders,
// 	 total: totalSelector(state)
// 	}
// )};
// export default connect(mapStateToProps, mapDispatchToProps)((
// 	Invoices
// ) as any);

// // export default Invoices;
