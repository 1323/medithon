// import { Button, Dropdown, Form, Input } from 'formik-semantic-ui';
// import * as React from 'react';
// import { connect } from 'react-redux';
// import { Table } from 'semantic-ui-react';
// import { createProductThunk, fetchProductsThunk } from '../redux/actions/';
// import styles from './Invoice.scss';

// // const mock = [{productName: 'a',
// // 	tax: 2,
// // 	unit: 'kg'}];
// const ProductForm = () => 
// 	// tslint:disable-next-line:no-unused-expression
// 	<Form.Children>
// 		<Form.Group widths="3">
// 			<Input label="Name" name="productName" placeholder="kasza" type="text" />
// 			<Dropdown
// 				label="Tax"
// 				name="tax"
// 				options={[ { text: '8%', value: 8 }, { text: '23%', value: 23 } ]}
// 			/>
// 			<Dropdown
// 				label="Unit"
// 				name="unit"
// 				options={[ { text: 'kg', value: 'kg' }, { text: 'instance', value: 'unit' } ]}
// 			/>
// 		</Form.Group>
// 		<Button.Submit> Submit </Button.Submit>
// 	</Form.Children>;

// const Product: React.SFC<{products:any, addProduct:any}> = ({products, addProduct}) => (
// <React.Fragment> 
// <h2>Products</h2>
// <div className={styles.tableContainer}>
// <Table columns={3}>
// 			<Table.Header>
// 				<Table.Row>
// 					<Table.HeaderCell>Name</Table.HeaderCell>
// 					<Table.HeaderCell>Tax</Table.HeaderCell>
// 					<Table.HeaderCell>Unit</Table.HeaderCell>
// 				</Table.Row>
// 			</Table.Header>

// 			<Table.Body>
// 			{products.map(item => (
// 					<Table.Row>
// 					<Table.Cell>{item.productName}</Table.Cell>
// 					<Table.Cell>{item.tax}</Table.Cell>
// 					<Table.Cell>{item.unit}</Table.Cell>
// 					</Table.Row>
// 				))}
// 			</Table.Body>

// 			<Table.Footer>
// 				<Table.Row>
// 					<Table.HeaderCell>value : </Table.HeaderCell>
// 					<Table.HeaderCell>value taxed: </Table.HeaderCell>
// 					<Table.HeaderCell />
// 				</Table.Row>
// 			</Table.Footer>
// 		</Table>
// 		</div>
// 	<Form
// 		initialValues={{
//       productName: '',
// 			tax: '',
// 			unit: '',
// 		}}
// 		// tslint:disable-next-line:jsx-no-lambda
// 		onSubmit={(values: any, { setSubmitting }: any) => {
// 			setTimeout(() => {
// 				console.log('values', values);
// 				addProduct(values)
// 				setSubmitting(false);
// 			}, 500);
// 		}}
// 		// tslint:disable-next-line:jsx-no-lambda
// 		render={() => <ProductForm />}
// 	/>
// </React.Fragment>
// );

// const mapDispatchToProps = (dispatch) => {
// 	return {
// 		addProduct: (formData) => {
// 			console.log('createProduct');
// 			dispatch(createProduct(formData));
// 		},
// 		fetchProduct: (input) => {
// 			dispatch(fetchProducts());
// 		},
// 	};
// };

// const mapStateToProps = (products: { products }) => (
// 	products.products
// );
// export default connect(mapStateToProps, mapDispatchToProps)((
// 	Product
// ) as any);

// // export default Product;