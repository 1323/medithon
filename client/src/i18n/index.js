import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';

i18n
		.use(LanguageDetector)
		.init({
				// we init with resources
				resources: {
						en: {
								translations: {
										assign_game_desc: 'Enrollment succeed',
										assign_game_title: "Don't be late for event",
										reject_game_title: 'Resignation succeed',
										reject_game_desc: "Please be more accurate next time",
										create_game_desc: "Well done !",
										save: "Save",
										updated_user_title: 'Changes were saved',
										create_game_title: 'Well done !',
										create_game_desc: "Successfuly created game proposition",
										errorTitle: 'Service problem occured',
										errorDescription: 'Try again later',
										fetch_games_title: '',
										starts: 'Starts',
										details: 'Details',
										matchIn: 'Match in',
										players: 'Players',
										tableTenis: 'Table tenis',
										dmrField: 'Domaradz football field',
										bulevoirScene: 'Scene on bulevoir',
										sandFieldPrz: 'Beachball field on campus',
										NickPlaceholder: 'please insert your nickname',
										FeedbackErrorHeader: 'E-mail validation error',
										FeedbackErrorContent: 'Try again',
										feedback: 'Tell us more',
										ctaPhrase: 'improve stamina',
										submitPhrase: 'check for friends',
										hallOfPodpromie: 'Hall of Podpromie',
										'⚽ piłka nożna': '⚽ Football'
								}
						},
						pl: {
								translations: {
										assign_game_desc: 'Nie spóznij się',
										assign_game_title: 'Zgłoszenie zostało przyjęte',
										reject_game_title: 'Rezygnacja została przyjęta',
										reject_game_desc: 'Następnym razem lepiej rozważyć decyzję lepiej',
										create_game_title: 'Udało się !',
										create_game_desc: "Dodałeś propozycję gry",
										updated_user_title: 'Udało się !',
										updated_user_desc: "Zmiana została zapisana",
										errorTitle: 'Chwilowo serwis niedomaga',
										errorDescription: 'Daj mu odpocząć i spróbuj za chwilę',
										fetch_games_title: '',
										games: 'gry',
										ctaPhrase: 'poprawisz kondycję',
										submitPhrase: 'ciekawe kto mógłby dołączyć...',
										at: 'o',
										fields: 'stadiony',
										save: 'Zapisz',
										Nickname: 'Nick',
										FeedbackErrorHeader: 'Póki co, niepoprawny email',
										FeedbackErrorContent: 'próbuj dalej',
										NickPlaceholder: 'Zapodaj swój nick',
										feedback: 'Napisz co myślisz',
										description: 'opis',
										tableTenis: 'Tenis stołowy',
										dyscypline: 'Dyscyplina',
										participants: 'uczestnicy',
										localization: 'Lokalizacja',
										about: 'menu',
										login: 'zaloguj',
										'Login for join': 'Zaloguj się aby dołączyć',
										Login: 'Zaloguj',
										logout: 'wyloguj',
										'Active games': 'Aktywne propozycje',
										'Create match': 'Dodaj wydarzenie',
										'Add match': 'Tworzenie wydarzenia',
										'Contact Us': 'Napisz do nas',
										Name: 'Nazwa',
										Discipline: 'Dyscyplina',
										football: 'Piłka nożna',
										Place: 'Lokalizacja',
										'please choose when': 'Tutaj wybierzesz kiedy',
										starts: 'Termin',
										details: 'szczegóły',
										matchIn: 'Mecz w ',
										players: 'Osób',
										Join: 'Dołącz',
										Resign: 'Zrezygnuj',
										Submit: 'Dodaj',
										Profile: 'Profil',
										Language: 'Język',
										higschoolNr2: 'Asfaltowe boisko Liceum nr 2',
										bulevoirScene: 'Scena przy bulwarach',
										stalTraining: 'Treningowy stadion Stali',
										sandFieldPrz: 'Boisko do plażówki PRZ',
										castleGrounds: 'Boisko pod zamkiem',
										zalesieField: 'Stadion Zalesie',
										dmrField: 'Orlik Domaradz',
										Bikes: 'Rowery',
										'Table tenis': 'Tenis stołowy',
										hallOfPodpromie: 'Hala Podpromie',
										'⚽ piłka nożna': '⚽ Piłka nożna'
								}
						}
				},
				fallbackLng: 'en',
				debug: false,
				// have a common namespace used around the full app
				ns: ['translations'],
				defaultNS: 'translations',

				keySeparator: false, // we use content as keys

				interpolation: {
						escapeValue: false, // not needed for react!!
						formatSeparator: ','
				},

				react: {
						wait: true
				}
		});

export default i18n;
