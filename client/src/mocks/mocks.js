export const disciplineOptions = [
	{
		key   : 'ft',
		value : 'football',
		text  : '⚽ piłka nożna'
	},
	{
		key   : 'tt',
		value : 'tableTenis',
		text  : 'Table tenis'
	},
	{
		key   : 'fr',
		value : 'frisbee',
		text  : 'Frisbee'
	},
	{
		key   : 'bk',
		value : 'bikes',
		text  : 'Bikes'
	}
];
// 50.0293926, 22.0011829

export const places = [
	{
		key         : 'h',
		value       : 'cjial6b0dlo3f01832qkqgn8m',
		activities  : [ 'football' ],
		coordinates : [ 50.041314, 21.994357 ],
		text        : 'higschoolNr2'
	},
	{
		key         : 'hp',
		value       : 'cjpras1pttjmt0183tq2o4k1m',
		coordinates : [ 50.0293926, 22.0011829 ],
		activities  : [ 'tableTenis' ],
		text        : 'hallOfPodpromie'
	},
	{
		key         : 's',
		value       : 'cjial5t4qlxxc019751vorb9c',
		coordinates : [ 50.019138, 21.985107 ],
		activities  : [ 'football' ],
		text        : 'sandFieldPrz'
	},
	{
		key         : 'b',
		value       : 'cjial4xlplxx30197delmj8u5',
		coordinates : [ 50.0257301, 22.001166 ],
		activities  : [ 'frisbee' ],
		text        : 'bulevoirScene'
	},
	{
		key         : 'st',
		value       : 'cjial4mn0lxwz019726080hjj',
		coordinates : [ 50.022414, 21.998694 ],
		activities  : [ 'football', 'frisbee' ],
		text        : 'stalTraining'
	},
	{
		key         : 'cg',
		value       : 'cji9ebptclf8q0197vd3gj842',
		coordinates : [ 50.032638, 22.00156 ],
		activities  : [ 'football', 'frisbee' ],
		text        : 'castleGrounds'
	},
	{
		key         : 'zal',
		value       : 'cjkh6g31d86iv0114a5a1gt52',
		coordinates : [ 50.003452, 22.030861 ],
		activities  : [ 'football', 'frisbee' ],
		text        : 'zalesieField'
	},
	{
		key         : 'dmr',
		value       : 'cjkh6gawf85sx0175g5l1pbyw',
		coordinates : [ 49.789669, 21.944489 ],
		activities  : [ 'football', 'frisbee' ],
		text        : 'dmrField'
	}
];

export const placesWithoutImage = {
	cjkh6gawf85sx0175g5l1pbyw : '/assets/fields/icon.svg',
	cjpras1pttjmt0183tq2o4k1m : '/assets/fields/icon.svg',
	cjkh6g31d86iv0114a5a1gt52 : '/assets/fields/icon.svg'
};

export const prepareData = (translate) => {
	return {
		places      : places.map((place) => ({
			...place,
			text : translate(place.text)
		})),
		disciplines : disciplineOptions.map((dis) => ({
			...dis,
			text : translate(dis.text)
		}))
	};
};

export const avatarList = [
	'alien.png',
	'a.png',
	'b.png',
	'c.png',
	'd.png',
	'e.png',
	'f.png',
	'g.png',
	'h.png',
	'i.png',
	'j.png',
	'k.png',
	'l.png',
	'm.png',
	'n.png',
	'o.png',
	'r.png',
	's.png',
	't.png',
	'u.png',
	'w.png'
];
