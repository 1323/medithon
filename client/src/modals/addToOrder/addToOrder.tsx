// import { Button, Form, Input } from 'formik-semantic-ui';
// import * as React from 'react';
// import { connect } from 'react-redux';
// // import { Table } from 'semantic-ui-react';
// import { MyTable } from '../../components/MyTable';

// import { addProductToInvoiceThunk, fetchProductsThunk } from '../../redux/actions/';

// interface IProps {
//   products?: any,
//   addProductToInvoice?: any,
//   fetchProduct?: any,
//   onCloseModal : any
// }
// interface IState {
//   selectedProduct : any
// }
// const ProductForm = ({selectedProduct, formProps}) => 
// 	// tslint:disable:no-unused-expression jsx-no-lambda
// 	<Form.Children>
// 		<Form.Group widths="3">
// 			<Input inputProps={ {
// 				disable:!selectedProduct,
// 				onChange: (ev, formValues) => {
// 				const {value} = formValues;
// 				const netto = value * formProps.values.price;
// 				formProps.setFieldValue('orderValue', (netto) + (netto * (selectedProduct.tax/100)))
// 				},
// 			 	type: 'number'
// 			}} 
// 				label="Quantity" name="quantity"/>
// 			<Input 
// 			inputProps={ {
// 				disable:!selectedProduct,
// 				onChange: (ev, formValues) => {
// 				const { value } = formValues;
// 				const netto = value * formProps.values.price;
// 				formProps.setFieldValue('orderValue',  (netto) + (netto * (selectedProduct.tax/100)))
// 				},
// 				type: 'number'}}
// 				label="Price" name="price" />
// 			<Input inputProps={
// 				{type: 'number', disable:!selectedProduct}
// 			} value={formProps.values.quantity * formProps.values.price} label="Order value" name="orderValue" />
// 		</Form.Group>
// 		<Button.Submit> Submit { } </Button.Submit>
// 	</Form.Children>;

// class Product extends React.Component < IProps,
// IState > {
// 	public state = {
//     selectedProduct: this.props.products[0]
// 	};
	
// public setSelectedProduct = (product) => {
// 	this.setState({selectedProduct: product})
// }

// public componentDidMount() {
// 	this.props.fetchProduct();
// }

// public render() {
// 	const {products} = this.props;
// 	return (
// 	<React.Fragment> 
// 	<h2>Products</h2>

// 	<MyTable
// 		headerData={['Name', 'Tax', 'Unit']}
// 		onSelectedItem={this.setSelectedProduct}
// 		bodyData={products}
// 		footerData={['value', 'value taxed', '']}
// 		columns={3}
// 	/>
// 		<Form
// 			initialValues={{
// 				orderValue: 1,
// 				price: 1,
// 				quantity: 1,
// 			}}
// 			// tslint:disable-next-line:jsx-no-lambda
// 			onSubmit={(values: any, { setSubmitting, setFieldError }: any) => {
// 					let isError = false;
// 					Object.keys(values).forEach(key => {
// 						switch (key) {
// 							case 'quantity':
// 								if (isNaN(values[key])) {
// 									setFieldError(key, 'numb Error')
// 									isError = true;
// 								}
// 								break;
// 							case 'price':
// 								if (isNaN(values[key])) {
// 									setFieldError(key, 'float Error')
// 									isError = true;
// 								}
// 								break;
// 							default:
// 								break;
// 						}
// 					});
// 					if(!isError) {
// 						this.props.onCloseModal()
// 						this.props.addProductToInvoice({product: this.state.selectedProduct, values});
// 						} 
// 					setSubmitting(false);
// 			}}
// 			// tslint:disable-next-line:jsx-no-lambda
// 			render={({...formProps}) => <ProductForm formProps={formProps} selectedProduct={this.state.selectedProduct} />}
// 		/>
// 	</React.Fragment>
// )}
// }

// const mapDispatchToProps = (dispatch) => {
// 	return {
// 		addProductToInvoice: (formData) => {
// 			console.log('addProductToInvoice');
// 			dispatch(addProductToInvoice(formData));
// 		},
// 		fetchProduct: (input) => {
// 			dispatch(fetchProducts());
// 		},
// 	};
// };

// const mapStateToProps = (products: { products }) => (
// 	products.products
// );
// export default connect(mapStateToProps, mapDispatchToProps)((
// 	Product
// ) as any);