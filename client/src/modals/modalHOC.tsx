// // tslint:disable:object-literal-sort-keys
// import * as React from 'react'
// import { Button, Modal, Transition } from 'semantic-ui-react';
// // import AddToOrder from './addToOrder';
// import './styles.scss';

// export interface IState {
// 	isOpen: boolean;
// }

// export interface IProps {
//   t?: any,
//   render: any,
//   buttonTitle: string,
//   buttonColor?: any,
//   headerTitle: string,
// }

// export default class CreateModal extends React.Component<IProps, IState> {
// 	constructor(props) {
// 		super(props);
// 		this.state = { isOpen: false };
// 	}

// 	public close = () => this.setState({ isOpen: false });

// 	public open = () => this.setState({ isOpen: true });

// 	public render() {
//     const { isOpen } = this.state;
//     const { buttonTitle, headerTitle, buttonColor = 'green' } = this.props;
// 		return (
// 			<div className="alignCenter">
// 				<Transition animation="scale" duration={500}>
// 					<Modal
// 						open={isOpen}
// 						onClose={this.close}
// 						trigger={
// 							<Button onClick={this.open} basic={true} color={buttonColor}>
// 								{buttonTitle}
// 							</Button>
// 						}
// 					>
// 						<Modal.Header>{headerTitle}</Modal.Header>
// 						<Modal.Content>
// 							{this.props.render({close: this.close})}
// 						</Modal.Content>
// 					</Modal>
// 				</Transition>
// 			</div>
// 		);
// 	}
// }
