import {ThunkAction} from 'redux-thunk';
import {addFeedback} from '../../../services/api/mutations/addFedback';

interface IState {
  state : any
}

const createFeedback : any = (formData) : ThunkAction < any,
IState,
any, any > => async(dispatch, getState, apolloClient) => {
  const {apolloClient: client} = apolloClient;
  const {email, feedback: content} = formData;
 
  const {data} = await client.mutate({
    mutation: addFeedback,
    variables: {
      email,
      content
    }
  });
  // dispatch(addGame(mapGame(data), 'type'));
};

export default createFeedback