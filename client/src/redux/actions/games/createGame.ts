import { ThunkAction } from 'redux-thunk';
import { createMatch } from '../../../services/api/mutations/createMatch';
import { mapGame } from '../../../services/api/mapers/gameMapper';

const createGame: any = (formData): ThunkAction<any, any, any, any> => async (
	dispatch,
	state,
	apolloClient: any
) => {
	const { apolloClient: client } = apolloClient;
	const { type, place, startsAt } = formData;
	const createGameMutation = {
		mutation: createMatch,
		variables: {
			type,
			creatorId: localStorage.getItem('user_id'),
			placeId: place,
			startsAt
		}
	};

	dispatch({
		type: 'CREATE_GAME',
		meta: {
			title: 'create_game_title',
			description: 'create_game_desc'
		},
		async payload() {
			const { data } = await client.mutate(createGameMutation);
			return mapGame(data);
		}
	});
};

export default createGame;
