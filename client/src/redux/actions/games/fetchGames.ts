import {ThunkAction} from 'redux-thunk';
import {GET_ALL_MATCHES_WITH_JOINED} from '../../../services/api/queries/getAllMatches';
import {mapGames} from '../../../services/api/mapers/gameMapper';

const fetchGames : any = (matchId) : ThunkAction < any,
		any,
		any,
		any > => async(dispatch, state, apolloClient : any) => {
				const {apolloClient: client, translate} = apolloClient;
				dispatch({
						type: 'FETCH_GAMES',
						async payload() {
								const {data} = await client.query({query: GET_ALL_MATCHES_WITH_JOINED});
								return mapGames(data);
						}
				});
		};
export default fetchGames;
