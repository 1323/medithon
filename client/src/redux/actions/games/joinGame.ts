import { ThunkAction } from 'redux-thunk';
import { addToMatchesUser } from '../../../services/api/mutations/joinMatch';

const joinGame: any = (input): ThunkAction<any, any, any, any> => async (
	dispatch,
	getState,
	apolloClient
) => {
	const { apolloClient: client } = apolloClient;
	const { joinedByUserId = localStorage.getItem('user_id'), joinedMatchesMatchId } = input;
	const joinGameMutation = {
		mutation: addToMatchesUser,
		variables: {
			joinedByUserId,
			joinedMatchesMatchId
		}
	};
	const user = getState().user;
	dispatch({
		type: 'ASSIGN_USER',
		meta: {
			title: 'assign_game_title',
			description: 'assign_game_desc'
		},
		async payload() {
			// TODO improve logic and redux passing
			const { data } = await client.mutate(joinGameMutation);
			return { joinedMatchesMatchId, user, ...data };
		}
	});
};

export default joinGame;
