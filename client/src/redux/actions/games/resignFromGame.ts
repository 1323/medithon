import { ThunkAction } from 'redux-thunk';
import { removeFromJoinedMatches } from '../../../services/api/mutations/removeFromJoinedMatches';

const ResignFromGame: any = (input): ThunkAction<any, any, any, any> => async (
	dispatch,
	getState,
	apolloClient
) => {
	const { apolloClient: client } = apolloClient;
	const { joinedByUserId = localStorage.getItem('user_id'), joinedMatchesMatchId } = input;
	const unAssignUserMutation = {
		mutation: removeFromJoinedMatches,
		variables: {
			joinedByUserId,
			joinedMatchesMatchId
		}
	};
	dispatch({
		type: 'UNASSIGN_USER',
		meta: {
			title: 'reject_game_title',
			description: 'reject_game_desc'
		},
		async payload() {
			await client.mutate(unAssignUserMutation);
			return { joinedMatchesMatchId, joinedByUserId };
		}
	});
};

export default ResignFromGame;
