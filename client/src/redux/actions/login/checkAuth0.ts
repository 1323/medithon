import {ThunkAction} from 'redux-thunk';
import {logoutThunk, fetchUserThunk} from '..'
// import apolloClient from '../../services/config/apollo';

interface IState {
  state : any;
}

const checkAuth : any = () : ThunkAction < any,
  IState,
  any,
  any > => async(dispatch, getState, {auth}) => {
    if (auth.isAuthenticated()) {
      dispatch(fetchUserThunk(localStorage.getItem('user_id')))
    } 
    else {
      dispatch(logoutThunk({}, 'type'))
    }
  };
export default checkAuth