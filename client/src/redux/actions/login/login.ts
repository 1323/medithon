import loginAuth from '../../../services/api/mutations/loginAuth';
import {loginSuccessfulThunk, auth0Thunk} from '..';
import {ThunkAction} from 'redux-thunk';
// import {loginAuth} from '../../../services/api/mutations/loginAuth';
interface IState {
  state : any;
}

const login : any = () : ThunkAction < any,
  IState,
  any,
  any > => (dispatch, getState, {
    apolloClient: client,
    auth
  }) => {
    const authSession = auth.handleAuthentication();
    if (authSession) {
      try {
        client
          .mutate({
          mutation: loginAuth,
          variables: {
            accessToken: authSession
          }
        })
          .then((data) => {
            const {
              data: {
                authenticateUser: {
                  id,
                  token,
                  name,
                  avatar,
                  favDisciplines
                }
              }
            } = data;
            
            localStorage.setItem('user_id', id);
            localStorage.setItem('id_token', token);
            dispatch(loginSuccessfulThunk({
              id,
              token,
              isLogged: true,
              name,
              avatar,
              favDisciplines
            }, 'type'));
          });
      } catch (err) {
      }
    } else {
      // dispatch(auth0Thunk());
    }
  };
export default login
