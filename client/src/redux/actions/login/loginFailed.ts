import { LOGIN_FAILED } from '../../constants';
import { createAction } from 'redux-actions';

interface ILoginFailedPayload {
	payload: any;
	type: any;
}

const loginFailed = createAction<
	ILoginFailedPayload,
	any,
	any
>(LOGIN_FAILED, (payload: Game | any, type: Game) => {
	if (payload.payload) {
		return { payload: payload.payload, type };
	} else {
		return { payload, type };
	}
});
export default loginFailed;
