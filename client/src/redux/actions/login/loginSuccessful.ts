import {LOGIN_SUCCESFULL} from '../../constants';
import {createAction} from 'redux-actions';

interface ILoginSuccessfulPayload {
  payload: any,
  type: any
}

const loginSuccessful = createAction < ILoginSuccessfulPayload,
  any,
  any > (LOGIN_SUCCESFULL, (payload : any | any, type : any) => {
    if (payload.payload) {
      return {payload: payload.payload, type}
    } else { 
      return {payload, type}
    }
    });
export default loginSuccessful