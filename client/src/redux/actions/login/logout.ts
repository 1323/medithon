import {LOG_OUT} from '../../constants';
import {createAction} from 'redux-actions';

interface ILogoutPayload {
  payload: any,
  type: any
}

const logout = createAction < ILogoutPayload,
  any,
  any > (LOG_OUT, (payload : Game | any, type : Game) => {
    if (payload.payload) {
      return {payload: payload.payload, type}
    } else { 
      return {payload, type}
    }
    });
export default logout