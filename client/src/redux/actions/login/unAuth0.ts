import { ThunkAction } from 'redux-thunk';
import {} from '../';
import { logoutThunk } from '../';
// import apolloClient from '../../services/config/apollo';

interface IState {
	state: any;
}

const unAuth: any = (): ThunkAction<any, IState, any, any> => async (dispatch, getState, { auth }) => {
	auth.logout();
	dispatch(logoutThunk({}, 'type'));
	// add action for handling redux
};
export default unAuth;
