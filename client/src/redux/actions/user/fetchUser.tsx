import {ThunkAction} from 'redux-thunk';
import {storeUserThunk} from '..';
import {GET_USER} from '../../../services/api/queries/getUser';

interface IState {
  state : any;
}

const fetchUser : any = (userId) : ThunkAction < any,
  IState,
  any, any > => async(dispatch, getState, apolloClient) => {
    const {apolloClient: client} = apolloClient;
    const { data: {User} } = await client.query({query: GET_USER, variables: {
      id: userId
    }});
    dispatch(storeUserThunk(User, 'type'));
  };
  export default fetchUser