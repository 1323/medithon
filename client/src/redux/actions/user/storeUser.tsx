import {STORE_USER} from '../../constants';
import {createAction} from 'redux-actions';

interface IStoreUserPayload {
  payload: any,
  type: any
}

const storeUser = createAction < IStoreUserPayload,
  any,
  any > (STORE_USER, (payload : any | any, type : any) => {
    if (payload && payload.payload) {
      return {payload: payload.payload, type}
    } else { 
      return {payload, type}
    }
    });
export default storeUser