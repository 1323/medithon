import {ThunkAction} from 'redux-thunk';
import {storeUserThunk} from '..';
import {updateUserMutation} from '../../../services/api/mutations/updateUserMutation';

interface IState {
  state : any;
}

const mapUpdateUser = (data) => {
  if(data) {
    const {updateUser: { name,
      favDisciplines,
      avatar,
      id,}
    } = data;
    return {
      name,
      favDisciplines,
      avatar,
      id,
    }
  }
  return {};
}

const updateUser : any = (input) : ThunkAction < any,
  IState,
  any,
  any > => async(dispatch, getState, apolloClient) => {
    const {apolloClient: client} = apolloClient;
    const {name, favDisciplines, avatar, id} = input;
    dispatch({
    type: 'UPDATE_USER',
    meta: {
			title: 'updated_user_title',
			description: 'updated_user_desc'
		},
		async payload() {
      const { data } = await client.mutate({
        mutation: updateUserMutation,
        variables: {
          name,
          favDisciplines,
          avatar,
          id
        }
      });
      return mapUpdateUser(data);
		}});
  };

export default updateUser