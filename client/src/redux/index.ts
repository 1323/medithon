import { Store, createStore, combineReducers, applyMiddleware } from 'redux';
import promise from 'redux-promise-middleware';
// import logger from "redux-logger";
import thunk from 'redux-thunk';
import { gamesReducer, userReducer, drawerReducer } from './reducer';
import alertMiddleware from './middleware/alertsService';
import apolloClient from '../services/config/apollo';
import Auth from '../services/login';
const auth = new Auth();
const thunkMiddleware = thunk.withExtraArgument({ apolloClient, auth });
const devtools: any = window.devToolsExtension ? window.devToolsExtension() : (f: any) => f;
const reducer = combineReducers<{}>({
	games: gamesReducer,
	user: userReducer,
	drawer: drawerReducer
});

const initialState = {};

const store: Store<{}> = applyMiddleware(alertMiddleware, promise(), thunkMiddleware)(devtools(createStore))(
	reducer,
	initialState
);

export default store;
