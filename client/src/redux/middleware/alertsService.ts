import isPromise from 'is-promise';
import _ from 'lodash';
import { translate, Trans } from "react-i18next"
import { toast } from 'react-semantic-toasts';
import i18n from '../../i18n';;

export default function alertsService(translateFn) {
  return next => action => {
    const types = {
      FETCH_GAMES: true,
      FETCH_GAMES_END: 'FETCH_GAMES_FULFILLED',
      FULFILLED: 'FULFILLED',
      REJECTED: 'REJECTED',
    };
    // const translate = translateFn;
 
    // If not a promise, continue on
    // if (!isPromise(action.payload)) {
    //   return next(action);
    // }
    if (action.meta) {
      // display only when match is created and updated profile 
      // when error occured display red one
      //
      let toastContent;
      if (action.type.includes(types.REJECTED)) {
        toastContent = {
            title: i18n.t('errorTitle'),
            description: i18n.t('errorDescription'),
        }
      } else if (action.meta.title && action.type.includes(types.FULFILLED)) {
        toastContent = {
          title: i18n.t(action.meta.title),
          description: i18n.t(action.meta.description),
        }
      }
      
      if (toastContent) {
        toast(
          toastContent,
          () => console.log('toast closed'),
          () => console.log('toast clicked')
        );
      }
      return next(action);
    }

    /*
     * Another solution would would be to include a property in `meta`
     * and evaulate that property.
     *
     * if (action.meta.globalError === true) {
     *   // handle error
     * }
     *
     * The error middleware serves to dispatch the initial pending promise to
     * the promise middleware, but adds a `catch`.
     */
    
    // if (_.has(types, action.type)) {
    //   console.log('promise_catched');
    //   // Dispatch initial pending promise, but catch any errors
    //   return next(action).catch(error => {
    //     console.warn(error);

    //     return error;
    //   });
    // }

    return next(action);
  };
}