import {handleActions} from 'redux-actions';
import {toggleDrawerThunk} from '../actions/'

interface IAction {
  type : string;
  payload?: any;
  error?: boolean;
  meta?: any;
}

interface IDraweState {
  isOpen: boolean
}

const initialState = {
    isOpen: false
  }

  const drawerReducer = handleActions < IDraweState > ({
      [toggleDrawerThunk.toString()]: (state : any, action : IAction): any => {
        return (
          {isOpen: !state.isOpen}
        )
      },
    },
  initialState)

  export default drawerReducer;