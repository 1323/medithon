// import {handleActions} from 'redux-actions';
import typeToReducer from 'type-to-reducer';

const CREATE_GAME = 'CREATE_GAME';
const ASSIGN_USER = 'ASSIGN_USER';
const UNASSIGN_USER = 'UNASSIGN_USER';
const FETCH_GAMES = 'FETCH_GAMES';

interface IGamesState {
	games: ReadonlyArray<Game>;
	isPending?: boolean;
	error?: false;
}

const initialState = {
	games: [],
	isPending: false,
	error: false
};

const gamesReducer = typeToReducer(
	{
		[FETCH_GAMES]: {
			PENDING: (state) => ({
				...initialState,
				isPending: true
			}),
			REJECTED: (state, action) => ({
				...initialState,
				isPending: false,
				error: action.payload
			}),
			FULFILLED: (state, action) => ({
				...initialState,
				isPending: false,
				games: action.payload
			})
		},
		[CREATE_GAME]: {
			PENDING: (state) => ({
				...state,
				isPending: false
			}),
			REJECTED: (state, action) => ({
				...state,
				error: action.payload
			}),
			FULFILLED: ({ games }, { payload }) => ({
				...initialState,
				games: [ ...games, payload ]
			})
		},
		[ASSIGN_USER]: {
			PENDING: (state) => ({
				...state,
				isPending: false
			}),
			REJECTED: (state, action) => ({
				...state,
				error: action.payload
			}),
			FULFILLED: ({ games, rest }, { payload: { joinedMatchesMatchId, user } }) => {
				const indx = games.findIndex((el) => el.id === joinedMatchesMatchId);
				return {
					...rest,
					games: Object.assign([], games, {
						[indx]: {
							...games[indx],
							isJoined: true,
							joinedBy: [ ... new Set([...games[indx].joinedBy, user]) ]
						}
					})
				};
			}
		},
		[UNASSIGN_USER]: {
			PENDING: (state) => ({
				...state
			}),
			REJECTED: (state, action) => ({
				...state,
				error: action.payload
			}),
			FULFILLED: ({ games, rest }, {payload: {joinedByUserId, joinedMatchesMatchId}}) => {
				const indx = games.findIndex((el) => el.id === joinedMatchesMatchId);
				return {
					...rest,
					games: Object.assign([], games, {
						[indx]: {
							...games[indx],
							isJoined: false,
							joinedBy: games[indx].joinedBy.filter(
								(item) => item.id !== joinedByUserId
							)
						}
					})
				};
			}
		}
	},
	initialState
);

export default gamesReducer;
