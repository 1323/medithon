import {handleActions} from 'redux-actions';
import typeToReducer from 'type-to-reducer';
import {loginThunk, logoutThunk, loginSuccessfulThunk, loginFailedThunk} from '../actions/'
import {STORE_USER, UPDATE_USER} from '../constants';
import { IAction } from 'src/types';

interface IUserState {
  isLogged: boolean
}

const initialState = {
    isLogged: false,
    id: null,
    token: null,
  }

  const userReducer = typeToReducer({
      [loginSuccessfulThunk.toString()]: (state : any, action : IAction): any => {
        return (
          action.payload.payload
        )
      },
      [UPDATE_USER]: {
          PENDING: (state) => ({
            ...state,
            isPending: true,
          }),
          REJECTED: (state, action) => ({
            ...state,
            isPending: false,
            error: action.payload
          }),
          FULFILLED: (state, { payload }) => 
            ({
            ...state,
            ...payload,
            isPending: false,
          })
      },
      [STORE_USER.toString()]: (state : any, action: IAction): any => {
        return (
          {...state, ...action.payload.payload, isLogged: true}
        )
      },
      [logoutThunk.toString()]: (state : any, action: IAction): any => {
        return (
          initialState
        )
      }
    },
  initialState)

  export default userReducer;