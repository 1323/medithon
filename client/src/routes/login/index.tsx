import React, { Component } from 'react';
import { Button, Loader } from 'semantic-ui-react';
import { connect } from 'react-redux';
import style from './style.scss';
import { Redirect } from 'react-router-dom';
import loginThunk from '../../redux/actions/login/login';

export interface IProps {
	render: any;
	onLogin: any;
}

export class Login extends Component<IProps, {}> {
	public componentDidMount() {
		this.props.onLogin();
	}

	public render() {
		return (
			<div className={style.home}>
				<Loader active inline="centered" />;
			</div>
		);
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		onLogin: () => {
			dispatch(loginThunk());
		}
	};
};
const mapStateToProps = (state: any) => ({
	games: state.games
});

export default connect(mapStateToProps, mapDispatchToProps)(Login as any);
