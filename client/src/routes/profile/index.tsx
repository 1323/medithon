import React, {Component} from 'react';
import {
		Segment,
		Image,
		Icon,
		Container,
		Loader,
		Grid,
		Form,
		Placeholder,
} from 'semantic-ui-react';
import 'react-semantic-toasts/styles/react-semantic-alert.css';
import {translate} from "react-i18next";
import {connect} from 'react-redux';
import style from './style.scss';
// import {Redirect} from 'react-router-dom';
import loginThunk from '../../redux/actions/login/login'
import updateUserThunk from '../../redux/actions/user/updateUser'
import { withRouter } from 'react-router-dom';

import {prepareData, avatarList} from '../../mocks/mocks';
export interface IProps {
		render : any;
		updateUser : any;
		t : any;
		onLogin : any;
		user : any;
}

export interface IState {
		avCounter
				?
				: number;
		favDisciplines
				?
				: string[];
		name?: string;
}

const mapDispatchToProps = {
			onLogin: loginThunk,
			updateUser: updateUserThunk,
}
const mapStateToProps = (state : any) => ({user: state.user});

@connect(mapStateToProps, mapDispatchToProps)

export class Profile extends Component < IProps,
IState > {
		public options : any;
		constructor(props) {
				super(props)
				this.options = prepareData(this.props.t)
				this.state = {
						avCounter: props.user.avatar,
						favDisciplines: [],
						name: props.user.name
				}
		}

		public componentWillReceiveProps(newProps) {
				if (newProps.user.name !== this.props.user.name) {
						this.setState({name: newProps.user.name})
				}
				if (newProps.user.avatar !== this.props.user.avatar) {
						this.setState({avCounter: newProps.user.avatar})
				}
		}

		public componentDidMount() {
				this
						.props
						.onLogin()
		}

		public handleChange = (e, {name, value}) => {
				this.setState({[name]: value})
		}

		public handleSubmit = async(values) => {
				const {name, avCounter: avatar, favDisciplines} = this.state
				const {updateUser, user} = this.props;
				// console.log('values', values);
				const id = localStorage.getItem('user_id');
				const input = {
						name,
						favDisciplines,
						id,
						avatar
				};
				const result = await updateUser(input)
		}

		public getNextAvatar = () => {
			console.log('clg next');
				const {avCounter} = this.state;
				if (avCounter < avatarList.length - 1) {
						this.setState((state => ({
								avCounter: state.avCounter + 1
						})))
				}
		}

		public getPreviousAvatar = () => {
				const {avCounter} = this.state;
				if (avCounter > 0) {
						this.setState((state => ({
								avCounter: state.avCounter - 1
						})))
				}
		}

		public getAvatar = (avId = 0) => {
				const {avCounter} = this.state;
				const values = Object.values(avatarList);
				return values[avCounter];
		}

		public render() {
				const {user, t} = this.props;
				const {name} = this.state;
				return (
						<Container className={style.container}>
								<Grid centered>
										<Grid.Row>
												<Grid.Column mobile={16} tablet={16} computer={8}>
														{!user.isLogged
																? <div className={style.home}><Loader active inline="centered"/>
																		</div>
																: <Segment size="tiny">
																		<Form autocomplete="off" onSubmit={this.handleSubmit} className={style.form}>
																			{ 
																				!this.getAvatar() ?
																				<Placeholder style={{ height: 150, width: 150, margin: '0 auto' }}>
																					<Placeholder.Image centered  size='small' circular/>
																				</Placeholder> :
																				<Image centered src={`assets/avs/${this.getAvatar()}`} size='small' circular/>
																				}
																				<div className={style.navContent}>
																						<Icon
																								centered
																								onClick={() => this.getPreviousAvatar()}
																								name='chevron left'/>
																						<Icon
																								centered
																								onClick={() =>{ 
																									this.getNextAvatar()}}
																								name='chevron right'/></div>

																				<Form.Field>
																						<label>{t('Nickname')}</label>
																						<Form.Input
																								name='name'
																								value={name}
																								onChange={this.handleChange}
																								placeholder={t('NickPlaceholder')}/>
																				</Form.Field>
																				{/* <Form.Field>
																<label>{t('Discipline')}</label>
																<Form.Select
																		onChange={this.handleChange}
																		name='discipline'
																		value={disciplines}
																		placeholder="⚽"
																		options={this.options.disciplines}/>
														</Form.Field> */}
																				<Form.Button disabled={!name} color="green" content={t('save')}/>
																		</Form>
																</Segment>
}
												</Grid.Column>
										</Grid.Row>
								</Grid>
						</Container>
				)
		}
}

export default withRouter(translate("translations")(Profile));
