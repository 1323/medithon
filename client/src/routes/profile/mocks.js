export const disciplineOptions = [
  {
    key: 'ft',
    value: 'football',
    text: '⚽'
  }, {
    key: 'fr',
    value: 'frisbee',
    text: 'Frisbee'
  }, {
    key: 'bk',
    value: 'bikes',
    text: 'Bikes'
  }
]

export const places = [
  {
    key: 'h',
    value: 'cjial6b0dlo3f01832qkqgn8m',
    text: 'higschoolNr2'
  }, {
    key: 's',
    value: 'cjial5t4qlxxc019751vorb9c',
    text: 'sandFieldPrz'
  }, {
    key: 'b',
    value: 'cjial4xlplxx30197delmj8u5',
    text: 'bulevoirScene'
  }, {
    key: 'st',
    value: ' cjial4mn0lxwz019726080hjj',
    text: 'stalTraining'
  }, {
    key: 'cg',
    value: 'cji9ebptclf8q0197vd3gj842',
    text: 'castleGrounds'
  }
]

export const prepareData = (translate) => {
  return {
    places: places.map(place => ({
      ...place,
      text : translate(place.text)
    })),
    disciplines: disciplineOptions.map(dis => ({
      ...dis,
      text : translate(dis.text)
    }))
  }
}

export const avatarList = [
  'alien.png',
  'a.png',
  'b.png',
  'c.png',
  'd.png',
  'e.png',
  'f.png',
  'g.png',
  'h.png',
  'i.png',
  'j.png',
  'k.png',
  'l.png',
  'm.png',
  'n.png',
  'o.png',
  'r.png',
  's.png',
  't.png',
  'u.png',
  'w.png',
]
