export const mapGames = (response) => {
		if (response && response.getAllMatchesWithJoinedFlag) {
				const {getAllMatchesWithJoinedFlag: {
								matches
						}} = response;

				return matches.map((match) => {
						const {
								players = '',
								joinedBy = [],
								type,
								isJoined = null,
								isActive,
								votes = 2,
								description = '',
								title = '',
								startsAt = '12',
								id,
								place = {}
						} = match;

						return {
								id,
								place,
								isJoined,
								isActive,
								joinedBy,
								startsAt,
								players,
								votes,
								description,
								title,
								type
						};
				});
		}
		return [];

};

// maping after creating match

export const mapGame = (response) => {
		const {createMatch} = response;

		const {
				players = '',
				joinedBy = [],
				isActive = true,
				isJoined = false,
				votes = 2,
				description = '',
				title = '',
				startsAt = '12',
				id,
				place = {},
				type
		} = createMatch;

		return {
				id,
				place,
				isJoined,
				joinedBy,
				isActive,
				startsAt,
				players,
				votes,
				description,
				title,
				type
		};
};
