import gql from 'graphql-tag';
export const addFeedback = gql `
mutation createFeedback($content:string!, $email:string!) {
    createFeedback(content: $content, email: $email) {
      id
    }
}
`;