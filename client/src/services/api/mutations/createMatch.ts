import gql from 'graphql-tag';
export const createMatch = gql `
mutation createMatch($description:String, $title:String, $type:String!, $creatorId: ID!, $placeId: ID!, $startsAt: DateTime!) {
    createMatch(description: $description, title: $title, type: $type, placeId: $placeId, creatorId: $creatorId, startsAt: $startsAt) {
      creator {
        id
      }
      title
      type
      description
      startsAt
      place {
        id
              name
              coordinates {
                lat
                lng
              }
      }
      joinedBy {
        email
        auth0UserId
        id
      }
      id
  }
}
`;