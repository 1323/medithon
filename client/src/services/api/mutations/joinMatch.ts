import gql from 'graphql-tag';
export const addToMatchesUser = gql `
mutation addToJoinedMatches($joinedByUserId: ID!
  $joinedMatchesMatchId: ID!) {
  addToJoinedMatches(joinedByUserId: $joinedByUserId, joinedMatchesMatchId: $joinedMatchesMatchId) {
    joinedByUser {
      id
    }
  }	
}
`;