import gql from 'graphql-tag';
const loginAuth = gql `
	mutation authenticateUser($accessToken:String!) {
		authenticateUser(accessToken: $accessToken) {
			id
			token
			name
			avatar
			favDisciplines
		}
	}
`;
export default loginAuth;