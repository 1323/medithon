import gql from 'graphql-tag';
export const removeFromJoinedMatches = gql `
mutation removeFromJoinedMatches($joinedByUserId: ID!
  $joinedMatchesMatchId: ID!) {
  removeFromJoinedMatches(joinedByUserId: $joinedByUserId, joinedMatchesMatchId: $joinedMatchesMatchId) {
    joinedByUser {
      id
    }
  }	
}
`;