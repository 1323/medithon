import gql from 'graphql-tag';
export const updateUserMutation = gql `
mutation updateUser($id: ID!
  $name:String! $favDisciplines: [String!], $avatar: Int! ) {
    updateUser(id: $id, favDisciplines: $favDisciplines, name: $name, avatar: $avatar ) {
      id,
      name,
      avatar,
      favDisciplines
  }	
}
`;