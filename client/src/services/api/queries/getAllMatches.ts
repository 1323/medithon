import gql from 'graphql-tag';

export const GET_ALL_MATCHES_WITH_JOINED = gql `
  query getAllMatchesWithJoinedFlag {
       getAllMatchesWithJoinedFlag {
         matches
     }
 }
`;