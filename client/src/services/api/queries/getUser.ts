import gql from 'graphql-tag';

export const GET_USER = gql `
query getUser($id: ID){
  User(id: $id){
    id,
    name,
    avatar,
    favDisciplines,
  }
}`