// Simple API:        https://api.graph.cool/simple/v1/cjgt3isjj5y870180ao905y0i
// Relay API:         https://api.graph.cool/relay/v1/cjgt3isjj5y870180ao905y0i
// Subscriptions API: wss://subscriptions.graph.cool/v1/cjgt3isjj5y870180ao905y0i

export const api =  'https://api.graph.cool/simple/v1/cjgt3isjj5y870180ao905y0i';