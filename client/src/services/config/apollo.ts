import ApolloClient from 'apollo-client';
import {createHttpLink} from 'apollo-link-http';
import {setContext} from 'apollo-link-context';
import {InMemoryCache} from 'apollo-cache-inmemory';  // waiting for better times with @model types in resolver =)
import { DefaultOptions } from 'apollo-client/ApolloClient';
const httpLink = createHttpLink({uri: 'https://api.graph.cool/simple/v1/cjgt3isjj5y870180ao905y0i'});

const authLink = setContext((_, {headers}) => {
		// get the authentication token from local storage if it exists let token =
		// localStorage.getItem('access_token');
		const token = localStorage.getItem('id_token');
		// let token =
		// 	'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJraW5kIjoic2NhcGhvbGQuc3VwZXJ1c2VyIiw
		// iZXhwIjo4NjQwMDAwMDAwMDAwMDAwLCJpYXQiOjE1MjQ3NzM4MzUsImF1ZCI6Ikp0Z2Z5WklRMnBKa
		// jlySThFOWU2MTdoUWNrMFJueEFuIiwiaXNzIjoiaHR0cHM6Ly9zY2FwaG9sZC5hdXRoMC5jb20vIiw
		// ic3ViIjoiNjkyYjM5OWEtYzZmOS00YjdhLWJkOWYtNzI2ODcyZDkyNWUyIn0.HfKCfjmMdfYIIGKRT
		// rFNLrhVYttPZj9fq-X12v7sna0';
		if (_.operationName === 'Login') { 
				return {
						headers: {
								...headers
						}
				};
		}
		
		// console.log() return the headers to the context so httpLink can read them
		return {
				headers: {
						...headers,
						Authorization: `Bearer ${token}`
				}
		};
});

const defaultOptions:DefaultOptions = {
		watchQuery: {
				fetchPolicy: 'network-only',
				errorPolicy: 'ignore'
		},
		query: {
				fetchPolicy: 'network-only',
				errorPolicy: 'all'
		}
}

const client = new ApolloClient({
		link: authLink.concat(httpLink),
		connectToDevTools: true,
		cache: new InMemoryCache({}),
		defaultOptions
});

export default client;