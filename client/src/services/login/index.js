import auth0 from 'auth0-js';
const hostName = window.location.hostname;
let rUri = 'https://1323.pl/profile';
if (hostName === 'localhost') {
	rUri = 'http://localhost:8080/profile';
}
export default class Auth {
	auth0 = new auth0.WebAuth({
		domain       : 'ttttt.eu.auth0.com',
		clientID     : 'xp0aDbu8UWcmdJwmb1tUYmArJMygHRHg',
		redirectUri  : rUri,
		audience     : 'https://ttttt.eu.auth0.com/api/v2/',
		responseType : 'token',
		scope        : 'openid'
	});

	handleAuthentication = () => {
		// return new Promise((resolve, reject) => {
		return this.auth0.parseHash((err, authResult) => {
			if (authResult && authResult.accessToken) {
				this.setSession(authResult);
				return authResult.accessToken;
			} else if (err) {
				return false;
			}
			return false;
		});
		// });
	};

	setSession = (authResult) => {
		// Set the time that the Access Token will expire at
		let expiresAt = JSON.stringify(authResult.expiresIn * 1000 + new Date().getTime());
		localStorage.setItem('access_token', authResult.accessToken);
		localStorage.setItem('id_token', authResult.idToken);
		localStorage.setItem('expires_at', expiresAt);
		// navigate to the home route history.replace('/home');
	};

	getSession = () => {
		const session = {
			access_token : localStorage.getItem('access_token'),
			id_token     : localStorage.getItem('id_token'),
			expires_at   : localStorage.getItem('expires_at')
		};
		return session;
	};

	logout = () => {
		// Clear Access Token and ID Token from local storage
		localStorage.removeItem('access_token');
		localStorage.removeItem('id_token');
		localStorage.removeItem('expires_at');
		localStorage.removeItem('user_id');
		// navigate to the home route history.replace('/');
	};

	isAuthenticated = () => {
		// Check whether the current time is past the Access Token's expiry time
		const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
		return new Date().getTime() < expiresAt;
	};

	login = () => {
		this.auth0.authorize();
	};
}
