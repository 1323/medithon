const CACHE_NAME = 'periodic-weather-v01';
const expectedCaches = [CACHE_NAME];

const translate = (name) => {
  switch (name) {
    case 'football':
      return 'piłke nożna';
    case 'bikes':
      return 'wypad rowerowy';
    case 'frisbee':
      return 'frisbee';
    default:
      return '';
  }
};

self.addEventListener('install', (event) => {
  self.skipWaiting();
});

// self.addEventListener('fetch', (event) => {   const url = new
// URL(event.request.url);   event.respondWith(
// caches.match(event.request).then(resp => resp ||
// fetch(event.request).then(response => caches.open(CACHE_NAME).then((cache) =>
// {         // if
// (event.request.url.match(/(locations|subscribe|notification)/) === null) { //
//   cache.put(event.request, response.clone());         // } return response;
// })     ))   ); });

self.addEventListener('push', (event) => {
  console.log('addinf push event');
  event.waitUntil(self.registration.pushManager.getSubscription().then((subscription) => {
    console.log('bf set subId');
    const subscriptionId = subscription
      .endpoint
      .replace('https://android.googleapis.com/gcm/send/', '', subscription);

    return fetch('https://not1.herokuapp.com/notification', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        Origin: 'https://not1.herokuapp.com',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({subscriptionId})
    }).then((response) => response.json()).then((data) => {
      if (data.value === null) {
        return false;
      }
      const title = `Nowe wydarzenie sportowe`;
      // const body = `${(data.value.body).substring(0, 32)}...`; const icon =
      // `./app/assets/icon/${data.value.icon}.jpg`;
      const badge = `/assets/${data.data.matches[0].content.type}.svg`;
      // const tag = data.value.tag;
      return self
        .registration
        .showNotification(title, {
          body: `${translate(data.data.matches[0].content.type)}`,
          icon: 'https://image.ibb.co/dCpvvU/logo.png',
          badge,
          tag: '',
          requireInteraction: true
        });
    });
  }));
});

self.addEventListener('activate', (event) => {
  event.waitUntil(caches.keys().then((keys) => Promise.all(keys.map((key) => {
    if (!expectedCaches.includes(key)) {
      return caches.delete(key);
    }
  }))).then(() => {
    // console.log(`${CACHE_NAME} now ready to handle fetches!`);
    return clients.claim();
  }));
});

self.addEventListener('notificationclick', (event) => {
  event
    .notification
    .close();
  event.waitUntil(clients.openWindow('https://localhost:8080'));
});