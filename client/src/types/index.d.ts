
export interface IUser {
	id: string;
	email: string;
}

export interface IAction {
  type : string;
  payload?: any;
  error?: boolean;
  meta?: any;
}

type ButtonColor = "black" | "blue" | "brown" | "green" | "grey" | "olive" | "orange" | "pink" | "purple" | "red" | "teal" | "violet" | "yellow" | "facebook" | "google plus" | "instagram" | "linkedin" | "twitter" | "vk" | "youtube";

declare global {
	interface Window { devToolsExtension: any; }

	type Game = {
		title: string;
		id: string;
		isJoined: boolean;
		joinedBy: IUser[];
		isActive: boolean;
		description: string;
		type: string;
		place: any;
		players: string[] | '';
		votes: number;
		startsAt: string;
	}
	
}
