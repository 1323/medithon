declare enum unit { Kg, Item }
declare enum supplierType { garmazerie, bread, groccery }

declare interface IProduct {
  name: string,
  tax: number,
  unit:  unit,
  id?: number,
} 

declare interface IOrder {
  product: IProduct,
  quantity: number,
  productPrice: number,
  value: number,
  id?: number,
};

declare interface Invoice {
  id: number,
  orders: IOrder[],
  total: number,
  supplierID: string,
  invoicingDate: Date,
}

declare interface ISupplier {
  nip: number,
  id: number,
  address: string,
  name: string,
  supplierType: supplierType,
}