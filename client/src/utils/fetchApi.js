// import fetch from 'unfetch';
// http://localhost:3000/
export default (url, body, callback) => {
	fetch(`https://not1.herokuapp.com/${url}`, {
		method  : 'POST',
		headers : {
			Accept         : 'application/json',
			Origin         : 'https://not1.herokuapp.com',
			'Content-Type' : 'application/json'
		},
		body    : JSON.stringify(body)
	})
		.then((response) => response.json())
		.then((data) => {
			callback(data);
		});
};
