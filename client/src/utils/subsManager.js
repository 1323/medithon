import fetch from 'unfetch';
import fetchApi from './fetchApi';

export default {
	create : (callback) => {
		navigator.serviceWorker.ready.then((serviceWorkerRegistration) => {
			serviceWorkerRegistration.pushManager
				.subscribe({ userVisibleOnly: true })
				.then((subscription) => {
					callback(
						subscription.endpoint.replace(
							'https://android.googleapis.com/gcm/send/',
							'',
							subscription
						)
					);
				})
				.catch((err) => {
					console.log('err', err, JSON.stringify(err));
				});
		});
	},
	toggle : (subscriptionId, callback) => {
		fetchApi(
			'subscribe',
			{
				subscriptionId
			},
			(data) => {
				callback(data);
			}
		);
	}
};
