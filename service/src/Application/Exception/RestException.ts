export class RestException extends Error {
    constructor(title: string, message: string, code: number = 404) {
        super(`\tERROR!!!\n\t\tTitle: ${title},\n\t\tCode: ${code},\n\t\tMessage: ${message}`);
    }
}