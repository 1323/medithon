export interface IImageModel {
    uuid: string;
    file: string;
    fullName: string;
    status: number;
    typeOfVisit: string;
}