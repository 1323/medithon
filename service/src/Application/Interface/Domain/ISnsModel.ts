export interface ISnsModel {
    uuid: string;
    sn: number;
    date: Date;
}