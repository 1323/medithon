import {isEmpty} from "lodash";
import {Db, MongoClient} from "mongodb";

export class MongoConnection {
    public static client: MongoClient;
    public static database: Db;
    protected options: any;

    constructor(options?: any) {
        this.options = options;
    }

    public async connect() {
        const url = this.options.url;
        const dbName = this.options.dbName;
        await (() =>  new Promise((resolve, reject) => {
            MongoClient.connect(url, function(err, client: MongoClient) {
                if (err) {
                    console.log(err.message);
                    reject(err);
                }

                MongoConnection.client   = client;
                MongoConnection.database = client.db(dbName);

                console.log(`Connected successfully to mongoDB server ( "${url + dbName}" ).`);
                resolve();
            });
        }))();
    }

    public static async query<T>(
        callback: (resolve: (value?: T | PromiseLike<T>) => void, reject: (reason?: any) => void) => void,
    ): Promise<T> {
        return await (() =>  new Promise(callback))();
    }

    public close() {
        if (isEmpty(MongoConnection.client) === false) {
            delete MongoConnection.database;
            MongoConnection.client.close();
        }
    }
}