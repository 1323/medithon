export class RestUri {

    public static getUri(url: string = "index", version: number = 1): string {
        return ("/api/rest/" + RestUri.version[version] + "/" + url);
    }
    protected static version: any = {
        1: "v1"
    };
}
