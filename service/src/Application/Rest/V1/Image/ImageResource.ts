import {DELETE, GET, Param, PATCH, Path, PathParam, POST} from "typescript-rest";
import {RestUri} from "../../RestUri";
import ImageModel, {IImage} from "../../../../Domain/ImageModel";
import {MongoConnection} from "../../../MongoDb/MongoConnection";
import {RestException} from "../../../Exception/RestException";
import {isEmpty} from 'lodash';

@Path(RestUri.getUri("image"))
export class ImageResource {

    @GET
    @Path(":uuid")
    public async getImage(
        @PathParam("uuid") uuid: string,
    ) {
        return await MongoConnection.query((resolve, reject) => {
            ImageModel.findOne({uuid: uuid}, (err, data) => {
                if (isEmpty(err)) {
                    resolve(data.toJSON());
                }
                throw new RestException('Image not found.', `Image not found for uuid '${uuid}'.`, 404);
            });
        });
    }

    @GET
    public async getCollection(
        @Param("count") count: number = 5,
        @Param("page") page: number = 1,
    ) {
        console.log('Start collection');
        return await MongoConnection.query(async (resolve, reject) => {
            console.log('In query page ' + (count * page) + ' count ' + page);
            await ImageModel.find()
                .skip(count * page)
                .limit(page)
                .sort({date: 'asc'})
                .exec((err, images) => {
                    console.log(err.name);
                    console.log(err.message);
                    console.log(images);
                    if (isEmpty(err)) {
                        resolve(images);
                    }
                    reject(err);
                });
        });
    }

    @POST
    public async addImage(
        @Param("file") file: string,
        @Param("fullName") fullName: string,
        @Param("typeOfVisit") typeOfVisit: string = 'online',
    ) {
        const image = new ImageModel({
            file: file,
            fullName: fullName,
            typeOfVisit: typeOfVisit,
            status: 0,
        });
        await image.save();

        return image.toJSON();
    }

    @PATCH
    @Path(":uuid")
    public async updateImage(
        @PathParam("uuid") uuid: string,
        @Param("fullName") fullName: string,
        @Param("typeOfVisit") typeOfVisit: string,
    ) {
        const image: IImage = await MongoConnection.query((resolve, reject) => {
            ImageModel.findOne({uuid: uuid}, (err, data) => {
                if (isEmpty(err)) {
                    resolve(data.toJSON());
                }
                throw new RestException('Image not found.', `Image not found for uuid '${uuid}'.`, 404);
            });
        });

        if (fullName) {
            image.fullName = fullName;
        }

        if (typeOfVisit) {
            image.typeOfVisit = typeOfVisit;
        }

        await image.save();

        return image.toJSON();
    }

    @DELETE
    @Path(":uuid")
    public async removeImage(
        @PathParam("uuid") uuid: string,
    ) {
        const image: IImage = await MongoConnection.query(resolve => {
            ImageModel.deleteOne({uuid: uuid}, err => {
                if (isEmpty(err)) {
                    resolve();
                }
                throw new RestException('Image not found.', `Image not found for uuid '${uuid}'.`, 404);
            });
        });
    }
}