import {GET, Param, Path, POST} from "typescript-rest";
import {RestUri} from "../../RestUri";

@Path(RestUri.getUri("image"))
export class SnsResource {

    @GET
    public async getCollection() {
        return "Get Collection";
    }

    @POST
    public async changeSns(
        @Param("collection") collection: string,
    ) {
        return "Add Image";
    }
}