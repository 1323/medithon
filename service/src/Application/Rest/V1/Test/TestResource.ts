import {GET, Param, Path} from "typescript-rest";
import {RestUri} from "../../RestUri";

@Path(RestUri.getUri("test"))
export class TestResource {

    @GET
    public async sayHello(
        @Param("name") name: string,
    ) {
        return "Hello " + name;
    }
}
