import { Document, Model, model, Schema, } from "mongoose";
import {IImageModel} from "../Application/Interface/Domain/IImageModel";

export interface IImage extends IImageModel, Document {}

export const ImageSchema: Schema = (new Schema({
    uuid: String,
    file: String,
    fullName: String,
    status: Number,
    typeOfVisit: String,
    date: Date,
})).pre("save", function(next) {
    this.set("date", new Date());
    next();
});

const ImageModel: Model<IImage> = model<IImage>("Image", ImageSchema);
export default ImageModel;