import { Document, Model, model, Schema} from "mongoose";
import {ISnsModel} from "../Application/Interface/Domain/ISnsModel";

export interface ISns extends ISnsModel, Document {}

export let SnsSchema: Schema = new Schema({
    uuid: String,
    sn: Number,
    date: Date,
});
SnsSchema.pre("save", function(next) {
    this.set("date", new Date());
    next();
});

export const Sns: Model<ISns> = model<ISns>("Sns", SnsSchema);