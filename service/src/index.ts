import * as dotenv from "dotenv";
import * as express from "express";
import {Server} from "typescript-rest";
import {MongoConnection} from "./Application/MongoDb/MongoConnection";
export * from "./Application/Rest/ResourceList";

dotenv.config();
const port: number =  process.env.SERVER_PORT ? Number(process.env.SERVER_PORT) : 3000;
const mongoUrl: string =  process.env.MONGOHQ_URL ? process.env.MONGOHQ_URL : "mongodb://localhost:27017/";
const mongoDB: string =  process.env.MONGOHQ_DB_NAME ? process.env.MONGOHQ_DB_NAME : "medithon";
const app: express.Application = express();
Server.buildServices(app);

app.listen(port, function() {
    // Connect mongoDB
    const mongoConnector: MongoConnection = new MongoConnection({url: mongoUrl, dbName: mongoDB});
    mongoConnector.connect();

    console.log( `Server started at http://localhost:${ port }` );
});
